﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Intenral
{
    /// <summary>
    /// Пользователь системы 
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор пользователя. Никогда не изменяется
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Логин, MaxLength = 64
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Email, MaxLength = 128
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Телефон, MaxLength = 16
        /// </summary>
        public string Phone { get; set; }
    }

}
