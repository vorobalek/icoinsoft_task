﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Intenral
{
    /// <summary>
    /// Интерфейс менеджера пользователей
    /// </summary>
    public interface IUserManager
    {
        /// <summary>
        /// Название
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Добавляет пользователя 
        /// </summary>
        /// <param name="user">Пользователь</param>
        void Add(User user);

        /// <summary>
        /// Уведомляет о том что у пользователя изменились данные. (Id - измениться не может)
        /// </summary>
        /// <param name="user">Пользователь</param>
        void Update(User user);

        /// <summary>
        /// Находит всех пользователей у которых query является подстрокой хотя бы одного из полей
        /// Login, Email, Phone. Без учета регистра.       
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <param name="maxCount">Максимальное кол-во записей которое должен вернуть запрос</param>
        /// <returns>Список пользователей в порядке возрастания Id, без дублей</returns>
        List<User> Find(string query, int maxCount);
    }
}
