﻿using System;

namespace Sample.Solve
{
    public static class Algorithms
    {
        public const ulong Mod = 257;

        public static ulong Pow(ulong number, uint pow)
        {
            unchecked
            {
                return (pow > 0) ? ((pow % 2 == 1) ? (number * Pow(number, pow - 1)) : Pow(number * number, pow / 2)) : 1;
            }
        }

        public static ulong Hash(string s, ulong mod)
        {
            ulong hash = 0;
            for (uint i = 0; i < s.Length; ++i)
            {
                hash += Pow(mod, i) * s[(int)i];
            }
            return hash;
        }

        public static ulong[] HashTable(string s, ulong mod)
        {
            ulong[] table = new ulong[Math.Max(s.Length, 1)];
            table[0] = s.Length > 0 ? s[0] : (ulong)0;
            for (uint i = 1; i < s.Length; ++i)
            {
                table[i] = table[i - 1] + Pow(mod, i) * s[(int)i];
            }
            return table;
        }

        public static ulong GetHash(ulong[] table, uint l, uint r)
        {
            ulong result = r > table.Length ? 0 :  table[r];
            if (l > 0)
            {
                result -= table[l - 1];
            }
            return result;
        }

        public static bool CheckSubstring(ulong[] tableA, string query)
        {
            var hashQuery = Hash(query, Mod);

            bool ok = false;
            for (uint i = 0; !ok && i + query.Length <= tableA.Length; ++i)
            {
                if (GetHash(tableA, i, i + (uint)query.Length - 1) == hashQuery * Pow(Mod, i))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
