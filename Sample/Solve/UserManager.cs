﻿using Sample.Intenral;
using System.Collections.Generic;

namespace Sample.Solve
{
    public class UserManager : IUserManager
    {
        class UserData
        {
            public User User { get; set; }
            public string Data { get; set; }

            public UserData(User user)
            {
                User = user;
                Data = $"{user.Login.ToLower()}\r\n{user.Email.ToLower()}\r\n{user.Phone.ToLower()}";
            }
        }
        SortedList<int, UserData> Data { get; set; } = new SortedList<int, UserData>();
        public string Name => $"Proval";

        public void Add(User user)
        {
            Data.Add(user.Id, new UserData(user));
        }

        public List<User> Find(string query, int maxCount)
        {
            query = query.ToLower();
            var list = new List<User>();
            uint count = 0;

            lock (Data)
            {
                foreach (var userData in Data.Values)
                {
                    if (count < maxCount)
                    {
                        if (userData.Data.Contains(query))
                        {
                            list.Add(userData.User);
                            ++count;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return list;
        }

        public void Update(User user)
        {
            lock (Data)
            {
                Data[user.Id] = new UserData(user);
            }
        }
    }
}
