﻿using Sample.Intenral;
using Sample.Solve;
using System;

namespace Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            IUserManager userManager = new UserManager();

            var random = new Random();

            for (int i = 0; i < 500000; ++i)
            {
                userManager.Add(new User()
                {
                    Id = i,
                    Login = random.Next().ToString(),
                    Email = random.Next().ToString(),
                    Phone = "",
                });
            }

            Console.WriteLine($"Query\tId\tLogin\tPhone\tEmail");

            for (int i = 0; i < 50000; ++i)
            {
                var query = random.Next(10000).ToString();
                var find = userManager.Find(query, 10);
                foreach (var item in find)
                {
                    Console.WriteLine($"{query}\t{item.Id}\t{item.Login}\t{item.Phone}\t{item.Email}");
                }
                if (find.Count > 0)
                {
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("-\t-\t-\t-\t-");
                }
            }
        }
    }
}
